import logging
from typing import Optional

import matplotlib
import matplotlib.colors as matcol
import matplotlib.pyplot as plt
import numpy as np
from astropy.visualization import astropy_mpl_style
from astropy.visualization import interval
from photutils.aperture import CircularAperture

matplotlib.use('Qt5Agg')
plt.style.use(astropy_mpl_style)

logging.getLogger("astroquery").setLevel(logging.ERROR)
logging.getLogger(__name__).setLevel(logging.DEBUG)

def plot_source(data: np.ndarray, source_positions_1: np.ndarray,
                source_positions_2: np.ndarray = None, source_positions_3: np.ndarray = None, psf: Optional[float]=None,
                blocking: bool = False):
    """
    Plot detected sources

    Args:
        data: np.ndarray, positional
            A numpy.array containing the data
        source_positions_1: np.ndarray, positional
            A np.ndarray containing the positions of all detected sources of an image [xcentroid, ycentroid]
        source_positions_2: np.ndarray, optional
            A np.ndarray containing the positions of all detected sources of another image[xcentroid, ycentroid]
        source_positions_3: np.ndarray, optional
            A np.ndarray containing the positions of all detected sources of another image[xcentroid, ycentroid]
    """
    if psf:
        r1= psf * 1.2
        r2 = psf * 1.3
        r3 = psf * 1.4
    else:
        r1= 2
        r2 = 2.5
        r3 = 3

    plt.figure()
    scale = interval.ZScaleInterval(nsamples=800, contrast=0.3, max_reject=0.5, min_npixels=5, krej=2.5,
                                    max_iterations=5)
    (vmin, vmax) = scale.get_limits(data)
    normalization = matcol.Normalize(vmin=vmin, vmax=vmax)
    plt.imshow(data, cmap='gray', origin='lower', norm=normalization, interpolation='nearest')
    plt.grid(visible=None)
    detected_sources_1 = CircularAperture(source_positions_1, r=r1)
    detected_sources_1.plot(color='yellow', lw=1.5, alpha=0.5)
    if source_positions_2 is not None:
        detected_sources_2 = CircularAperture(source_positions_2, r=r2)
        detected_sources_2.plot(color='green', lw=1.5, alpha=0.5)
    if source_positions_3 is not None:
        detected_sources_3 = CircularAperture(source_positions_3, r=r3)
        detected_sources_3.plot(color='red', lw=1.5, alpha=0.5)
    plt.show(block=blocking)