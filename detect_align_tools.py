import logging
from typing import Tuple, Optional

import astropy.io.fits
import astropy.units as u
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from astropy.convolution import Gaussian2DKernel, convolve_fft
from astropy.coordinates import SkyCoord
from astropy.stats import sigma_clipped_stats
from astropy.units import Quantity
from astropy.visualization import astropy_mpl_style
from astropy.wcs import WCS
from astroquery.gaia import Gaia
from photutils.detection import DAOStarFinder

import psf_tools

matplotlib.use('Qt5Agg')
plt.style.use(astropy_mpl_style)

logging.getLogger("astroquery").setLevel(logging.ERROR)
logging.getLogger(__name__).setLevel(logging.DEBUG)


def source_detection(data: np.ndarray, psf_size: float, sigma: float = 3.0, threshold: float = 5.0,
                     saturation: bool = False) -> Tuple[np.ndarray, np.ndarray]:
    """
    Detect point sources in an image
    Args:
        data : np.ndarray, positional
                A numpy.array containing the data of a science extension -->  HDU['SCI'].data
        threshold: float, positional
                The absolute image value above which to select sources.
        psf_size : float, positional
                The psf size in arcsec (usually x2)
        sigma: float, optional
                The truncation radius of the Gaussian kernel in units of sigma (standard deviation)
                [1 sigma = FWHM / (2.0*sqrt(2.0*log(2.0)))].
        saturation: bool, optional
        TODO Complete this comment
                Set to True in case of saturated sources in a given image
                The given saturated data convolved with data gaussian kernel before the source determination

    Returns:
        convolved_data: np.ndarray containing the data used for source determination
        positions: np.ndarray containing the positions of all detected sources [xcentroid, ycentroid]
    """
    if saturation:
        print('Detecting saturated & non-saturated sources...')
        data_sat = data.copy()
        kernel = Gaussian2DKernel(x_stddev=5)
        convolved_data = convolve_fft(data_sat, kernel)
        daofind = DAOStarFinder(fwhm=psf_size * 4, threshold=threshold, roundlo=-0.6, roundhi=0.6)
        sources_sat = daofind(convolved_data)
        if sources_sat is None:
            raise ValueError('No sources detected in the image. positions array is None')

        for col in sources_sat.colnames:
            sources_sat[col].info.format = '%.8g'  # for consistent table output

        positions = np.transpose((sources_sat['xcentroid'], sources_sat['ycentroid']))
        print("Saturated & non-saturated sources detected")
    else:
        print('Detecting non-saturated sources...')
        data_nonsat = data.copy()
        convolved_data = data_nonsat
        mean, median, std = sigma_clipped_stats(convolved_data, sigma=sigma)
        daofind = DAOStarFinder(fwhm=psf_size, threshold=threshold, roundlo=-0.6, roundhi=0.6)
        sources = daofind(convolved_data - median)

        if sources is None:
            raise ValueError('No sources detected in the image. positions array is None')

        for col in sources.colnames:
            sources[col].info.format = '%.8g'  # for consistent table output
        positions = np.transpose((sources['xcentroid'], sources['ycentroid']))
        print("Non-saturated sources detected")

    return convolved_data, positions


def gaia_sources(header: astropy.io.fits.header.Header) -> np.ndarray:
    """
    Searches for all the sources contained :
     - in a squared region of some degrees --> defined in the Quantity() function
     - around a specific point in RA/Dec coordinates --> defined in the SkyCoord() function

    Args:
        header: astropy.io.fits.header.Header object, positional
                The header of the science extension of a FITS file -->  HDU['SCI'].header
    Returns:
        positions_gaia : np.ndarray
            A np.ndarray array with all Gaia DR3 sources contained in the selected region
            around the selected point in RA/Dec coordinates.
    """
    print('Retrieving Gaia DR3 sources...')
    w = WCS(header)
    coord = SkyCoord(ra=header['CRVAL1'], dec=header['CRVAL2'], unit=(u.deg, u.deg))
    # define telescope radius
    radius = Quantity(np.sqrt(header['PIXAR_A2']) * header['NAXIS1'], u.arcsec)  # for JWST telescope
    # get gaia positions
    Gaia.ROW_LIMIT = -1
    Gaia.MAIN_GAIA_TABLE = "gaiadr3.gaia_source"  # Reselect Data Release 3, default
    gaia_query = Gaia.query_object_async(coordinate=coord, radius=radius)
    reduced_query = gaia_query['ra', 'dec', 'phot_g_mean_mag']
    ra_dec_pix = w.world_to_pixel(SkyCoord(ra=reduced_query['ra'], dec=reduced_query['dec'], unit=(u.deg, u.deg)))
    positions_gaia = np.transpose((ra_dec_pix[0], ra_dec_pix[1]))
    if positions_gaia is None:
        print('Gaia DR3 sources not found in this region')
        raise ValueError(' positions_gaia array is None')
    print('Gaia DR3 source positions retrieved')
    return positions_gaia


def common_sources(positions1: np.ndarray, positions2: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Detect common sources using vector distances

    Args:
        positions1: np.ndarray, positional
            A np.ndarray containing the positions of all detected sources of an image [xcentroid, ycentroid]
        positions2: np.ndarray, positional
            A np.ndarray containing the positions of all detected sources of another image[xcentroid, ycentroid]

    Returns:
        jwst_common: np.ndarray
            A list of np.ndarray containing the jwst common source positions
        gaia_common: np.ndarray
            A list of np.ndarray containing the Gaia DR3 common source positions
    """

    gaia_common, jwst_common = [], []
    for i in positions1:
        numb = 0
        for j in positions2:
            if np.sqrt(abs(j[0] - i[0]) ** 2 + abs(j[1] - i[1]) ** 2) < 30:
                numb += 1
                for h in positions2:
                    if j[0] - 5 < h[0] < j[0] + 5 and j[1] - 5 < h[1] < j[1] + 5 and j[0] != h[0] and j[1] != h[1]:
                        numb += 1
                if numb > 1:
                    pass
                else:
                    jwst_common.append(j)
                    gaia_common.append(i)
        if jwst_common is None or gaia_common is None:
            print('No common sources')
            raise ValueError('common array is None')
    return np.array(jwst_common), np.array(gaia_common)


def median_offset(positions_jwst_xy: np.ndarray, positions_gaia_xy: np.ndarray,
                  head: astropy.io.fits.header.Header) -> np.ndarray:
    """
    # Calculate the median difference of the given positions for each coordinate ( in [ra, dec])

    Args:
        positions_jwst_xy: np.ndarray, positional
            A np.ndarray containing the positions of all detected sources in a jwst image
        positions_gaia_xy:  np.ndarray, positional
             A np.ndarray array with all Gaia DR3 source positions contained in the jwst image
        head: astropy.io.fits.header.Header object, positional
                The header of the science extension of the jwst FITS file -->  HDU['SCI'].header

    Returns: np.ndarray
            A numpy array containing the median position values

    Algorythm:
        1. read wcs information from header
        2. transform the given position values from pixel to wcs
        3. For each position calculate the difference between the given jwst and gaia coordinates [gaia - jwst]
        4. Calculate the median difference of (3) for all source positions for each coordinate
    """
    w = WCS(head)
    positions_jwst_radec = np.array([w.pixel_to_world(i[0], i[1]) for i in positions_jwst_xy])
    positions_gaia_radec = np.array([w.pixel_to_world(i[0], i[1]) for i in positions_gaia_xy])
    dif = [[(positions_gaia_radec[i].ra - positions_jwst_radec[i].ra).value,
            (positions_gaia_radec[i].dec - positions_jwst_radec[i].dec).value]
           for i in range(len(positions_jwst_radec))]
    median_dif = np.median(dif, axis=0)
    return median_dif


def get_source_positions(data: np.ndarray,
                         head_sci: astropy.io.fits.Header,
                         threshold: float,
                         psf: float,
                         sigma: Optional[int] = 3,
                         saturation: Optional[bool] = False) -> dict[str: np.ndarray]:
    """
    Create a dictionary containing source positions detected on the given image

     Args:
        data: Path, positional
            The path of the level2 file (ie '*_cal.fits' )
        head_sci: astropy.io.fits.Header, positional
            The header of the science unit
        threshold: float, positional
                The absolute image value above which to select sources.
        psf : float, positional
                The psf size in arcsec (usually x2)
        sigma: float, optional
                The truncation radius of the Gaussian kernel in units of sigma (standard deviation)
                [1 sigma = FWHM / (2.0*sqrt(2.0*log(2.0)))].
        saturation: bool, optional

    Returns:
        dict: dict
        A dictionary containing several position coordinates
    """

    try:
        # detect all source positions in the data
        convolved_data, sources_xy = source_detection(data, psf, sigma=sigma, threshold=threshold,
                                                      saturation=saturation)
    except ValueError as e:
        print('No sources detected in the image. positions array is None')
        raise e
    try:
        # extract all Gaia DR3 source positions containing in the data region
        gaia_xy = gaia_sources(head_sci)
    except ValueError as e:
        print('Gaia DR3 source not found. positions_gaia array is None')
        raise e
    try:
        # extract the position of the common sources between Gaia DR3 and jwst
        sources_common_xy, gaia_common_xy = common_sources(gaia_xy, sources_xy)
    except ValueError as e:
        print('Gaia DR3 source not found. positions_gaia array is None')
        raise e

    extracted_positions = {'sources_xy': sources_xy,
                           'gaia_xy': gaia_xy,
                           'sources_common_xy': sources_common_xy,
                           'gaia_common_xy': gaia_common_xy}

    return extracted_positions

