import numpy as np

def get_psfsize(head) -> float:
    """
    Calculate the size of the psf
    Args:
        head: astropy.io.fits.header.Header object, positional
                The header of the primary extension of a FITS file --> HDU['PRIMARY'].header
    Returns:
        psf_arcsec: float
            The size of the psf in [arc-sec]
    """
    # define the telescope diameter
    if head['TELESCOP'] == 'HST':
        diameter = 2.4 # HST diameter
    else:
        diameter = 6.5  # JWST diameter
    # define the filter id
    # check if PUPIL have been used for NIRCAM observations
    if 'PUPIL' in head.keys() and head['PUPIL'] != 'CLEAR':
        filter_id = head['PUPIL']
    else:
        filter_id = head['FILTER']
    # calculate wavelength in [micro-meters]
    wave0 = int("".join(filter(str.isdigit, filter_id))) / 100  #TODO make it in a better way
    # raw determination of the psf in [arc-sec]
    psf_arcsec = wave0 * 1e-6 / diameter / (2 * np.pi / (360 * 3600))
    return psf_arcsec

def get_psf(head_0, head_sci):
    psf_arcsec = get_psfsize(head_0)
    if 'CDELT1' in head_0.keys():
        psf = psf_arcsec / (3600 * head_0['CDELT1']) * 2
    else:
        psf = psf_arcsec / (np.sqrt(head_sci['PIXAR_A2'])) * 2
    return psf