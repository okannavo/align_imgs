import argparse
import logging
from dataclasses import dataclass
from pathlib import Path
from typing import List
from typing import Optional

import astropy
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from astropy.io import fits
from astropy.visualization import astropy_mpl_style

import detect_align_tools
import plot_detected_sources
import psf_tools

matplotlib.use('Qt5Agg')
plt.style.use(astropy_mpl_style)

logging.getLogger("astroquery").setLevel(logging.ERROR)
logging.getLogger(__name__).setLevel(logging.DEBUG)


def correct_align(head: astropy.io.fits.Header, median_dif: np.ndarray):
    """ Update the science header ( i.e. HDU['SCI'].header )
    This update is made in order to align source position of a given jwst image with these of the Gaia DR3

    Args:
        head: positional, astropy.io.fits.Header
            The science header of a fits file
        median_dif : positional, np.ndarray
            A np.ndarray of shape (2,)
            containing the median difference of source position for each coordinate between jwst and gaia
    Notes:
        1. The median difference value in each coordinate is added in the following variables of the science header:
            - CRVAL1 : first axis value at the reference pixel
            - CRVAL2 : second axis value at the reference pixel
        2. The median applied values are noted in following variables of the science header:
            SHIFT_RA :
            SHIFT_DE :
"""
    head['CRVAL1'] = head['CRVAL1'] + median_dif[0]
    head['CRVAL2'] = head['CRVAL2'] + median_dif[1]
    head['SHIFT_RA'] = median_dif[0]
    head['SHIFT_DE'] = median_dif[1]


@dataclass
class AlignImage:
    threshold: float
    sigma: Optional[int] = 3
    saturation: Optional[bool] = False
    plot_sources: Optional[bool] = False
    plot_alignment: Optional[bool] = False
    save_align: Optional[bool] = True
    save_positions: Optional[bool] = True
    output_dir: Optional[Path] = Path.cwd()
    common_positions: Optional[Path] = None

    @classmethod
    def build_from_args(cls) -> tuple["AlignImage", Path]:
        parser = argparse.ArgumentParser()
        parser.add_argument('file_or_dir', help='A level2 file to be aligned (<*cal.fits>)')
        parser.add_argument('threshold', type=float, help='The absolute image value above which to select sources')
        parser.add_argument('-s', '--sigma', type=float, default=3.0)
        parser.add_argument('--plot-sources', action="store_false")
        parser.add_argument('--plot-alignment', action="store_false")

        parser.add_argument('--saturation', action="store_true")
        parser.add_argument('--output-dir')
        parser.add_argument('-a', '--save-align', action="store_false")
        parser.add_argument('-p', '--save-positions', action="store_false")
        parser.add_argument('-c', '--common-positions', type=str)

        args = parser.parse_args()
        file_or_dir = Path(args.file_or_dir)

        if args.common_positions is not None:
            common_positions = Path(args.common_positions)
        else:
            common_positions = args.common_positions

        if args.output_dir is None:
            if file_or_dir.is_file():
                output_dir = Path(file_or_dir).parent
            elif file_or_dir.is_dir():
                output_dir = Path(args.file_or_dir)
            else:
                raise ValueError('')
        else:
            output_dir = Path(args.output_dir)

        return cls(
            threshold=args.threshold,
            sigma=args.sigma,
            plot_sources=args.plot_sources,
            plot_alignment=args.plot_alignment,
            output_dir=output_dir,
            saturation=args.saturation,
            save_align=args.save_align,
            common_positions=common_positions,
            save_positions=args.save_positions
        ), file_or_dir

    def do_alignment(self, file_to_align: Optional[Path]) -> np.ndarray:

        hdu = fits.open(file_to_align)
        head_0 = hdu['PRIMARY'].header
        data = hdu['SCI'].data
        head_sci = hdu['SCI'].header
        psf = psf_tools.get_psf(head_0, head_sci)

        positions_init = detect_align_tools.get_source_positions(data, head_sci,
                                                                 self.threshold, psf, self.sigma, self.saturation)

        if self.common_positions:
            # read the array containing all source positions from the given yaml file and split it
            # in order to obtain two arrays containing sources_common_xy and gaia_common_xy coordinates
            with open(self.common_positions, 'r') as f:
                file_content = yaml.safe_load(f)
            position_data = np.array(file_content['data'])
            print(data.shape)
            sources_common_xy, gaia_common_xy = np.hsplit(position_data, 2)
            sources_common_df = pd.DataFrame(sources_common_xy, columns=['x_common', 'y_common'])
            gaia_common_df = pd.DataFrame(gaia_common_xy, columns=['x_gaia', 'y_gaia'])
            print(gaia_common_xy.shape)
        else:
            # load the sources_common_xy and gaia_common_xy positions and create a DataFrame for each of them
            sources_common_xy = positions_init['sources_common_xy']
            sources_common_df = pd.DataFrame(sources_common_xy, columns=['x_common', 'y_common'])
            gaia_common_xy = positions_init['gaia_common_xy']
            gaia_common_df = pd.DataFrame(gaia_common_xy, columns=['x_gaia', 'y_gaia'])

        print('------------------------------------------------------------------')
        print('Approximate positions of common sources used for alignment [in pixels] :')
        print(sources_common_df)
        print('------------------------------------------------------------------')

        if self.save_positions:
            position_file_name = self.output_dir / f'{file_to_align.stem}_common_positions.yml'
            # Add all positions to a single DataFrame
            common_df = pd.concat([sources_common_df, gaia_common_df], axis=1)
            with open(position_file_name, 'w') as file:
                yaml.dump(common_df.to_dict(orient='split'), file, default_flow_style=False)
                print(f'Common detected positions have been saved in {position_file_name}')

        if self.plot_sources:
            plot_detected_sources.plot_source(data, positions_init['sources_xy'], positions_init['gaia_xy'],
                                              sources_common_xy, psf, blocking=False)

        # median offset calculation for x and y coordinates
        median_dif = detect_align_tools.median_offset(sources_common_xy, gaia_common_xy, head_sci)
        correct_align(head_sci, median_dif)  # modify science header
        print('-------------------------------------------------------------------')
        print('File has been aligned using the median offset values:')
        print(f'SHIFT_RA [RA] : {median_dif[0]}, SHIFT_DEC [DEC] : {median_dif[1]}')
        print('-------------------------------------------------------------------')

        if self.plot_alignment:
            # align_hdu = fits.open(aligned_file)
            # aligned_data = align_hdu['SCI'].data
            positions_aligned = detect_align_tools.get_source_positions(data, head_sci,
                                                                        self.threshold, psf, self.sigma,
                                                                        self.saturation)
            print('Aligned image positions extracted')

            plot_detected_sources.plot_source(data, positions_aligned['sources_xy'],
                                              positions_aligned['gaia_xy'],
                                              sources_common_xy, psf, blocking=True)

        if self.save_align:
            aligned_file = self.output_dir / f'{file_to_align.stem}_aligned.fits'
            hdu.writeto(aligned_file, overwrite=True)
            print(f'Aligned image saved in {aligned_file}')
            hdu.close()

        return median_dif

    def align_image_or_dir(self, file_or_dir: Path):
        if file_or_dir.is_file():
            return self.do_alignment(file_or_dir)
        elif file_or_dir.is_dir():
            median_values = [self.do_alignment(f) for f in file_or_dir.iterdir() if '.fits' in f.name]
            df = pd.DataFrame(median_values, columns=['X', 'Y'])
            # force scientific notation
            pd.options.display.float_format = '{:12.5e}'.format

            print(df)
            return median_values


@dataclass
class MultiAlignment():
    src_dir: Path
    align_with_dir: Optional[Path]  # A path containing images for median offset calculation
    threshold: Optional[float] = None
    output_dir: Optional[Path] = None
    median_dif: Optional[List[float]] = None

    @classmethod
    def build_from_args(cls) -> "MultiAlignment":
        parser = argparse.ArgumentParser()
        parser.add_argument('src_dir')
        parser.add_argument('align_with_dir')
        parser.add_argument('-o', '--output-dir')
        parser.add_argument('-d', '--median-dif', nargs='*')

        args = parser.parse_args()
        align_with_dir = Path(args.align_with_dir)
        if args.output_dir is None:
            output_dir = Path(align_with_dir).parent
        else:
            output_dir = Path(args.output_dir)

        median_dif = None
        if args.median_dif:
            median_dif = [float(i) for i in args.median_dif]

        return cls(
            src_dir=Path(args.src_dir),
            align_with_dir=align_with_dir,
            output_dir=output_dir,
            median_dif=median_dif
        )

    @property
    def filelist_to_align(self) -> list[Path]:
        """
        Create a list of already aligned files for mean offset calculation of source positions

        Returns: Optional[list[Path]]
            A list with the paths of all aligned '*cal' files containing in the given directory <align_with_dir>

        """
        return [file for file in self.align_with_dir.iterdir() if 'cal.fits' in file.name]

    @property
    def mean_offset(self) -> np.ndarray:
        """Calculate the mean offset of source position using the header of the aligned images in the given list

        Returns: np.ndarray
            A np.ndarray of shape (2,) with the mean offset for each coordinate
        """
        if self.median_dif:
            return np.array(self.median_dif)

        offsets = []
        for file in self.filelist_to_align:
            hdu = fits.open(file)
            shift_ra = float(hdu['SCI'].header['SHIFT_RA'])
            shift_de = float(hdu['SCI'].header['SHIFT_DE'])
            dif = np.array([shift_ra, shift_de])
            offsets.append(dif)
        np.array(offsets)
        print(np.mean(offsets, axis=0))
        return np.mean(offsets, axis=0)

    def apply_alignment_to_all_files(self):
        median_dif = self.mean_offset
        print(median_dif)

        for cal_file in self.src_dir.iterdir():
            hdu = fits.open(cal_file)
            head_sci = hdu['SCI'].header
            correct_align(head_sci, median_dif)

            self.output_dir.mkdir(parents=True, exist_ok=True)
            aligned_file = self.output_dir / f'{cal_file.stem}_aligned.fits'
            hdu.writeto(aligned_file, overwrite=True)
            print(cal_file.name)
            hdu.close()
        print('-------------------------------------------------------------------')
        print('All cal files have been aligned using the mean median differences:')
        print(f'SHIFT_RA [RA] : {median_dif[0]}, SHIFT_DEC [DEC] : {median_dif[1]}')
        print('-------------------------------------------------------------------')


def apply_multi_align_cli():
    update_align = MultiAlignment.build_from_args()
    update_align.apply_alignment_to_all_files()

if __name__ == '__main__':
    align_img, file_or_dir = AlignImage.build_from_args()
    align_img.align_image_or_dir(file_or_dir)



