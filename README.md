To align an image: 

```python
$ python alignment.py <file_to_align> <threshold>
```

To align images using median offset values call the apply_multi_align_cli() function 

